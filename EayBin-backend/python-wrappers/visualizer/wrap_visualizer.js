export default class WrapVisualizer {
    // TODO these are the parameters
    constructor(inp = null) {
        this.input = inp;
    }

    exec(){
        console.log('Running visualizer Tool...');
        return new Promise(resolve => resolve({step: 'visualizer', output: 'visualizer Results'}));
    }
}