import cmd from 'node-cmd';
import fs from 'fs';
import path from 'path';
import uuidv4 from 'uuid/v4';

export default class WrapAbundanceBin {
    inputFilename;
    kmer_len;
    //output;
    exclude;
    exclude_max;
    bin_num;
    method;

    // TODO these are the parameters
    constructor(inp = null) {
        for (let param of inp) {
            console.log("obejct-->", param);
            switch (param.name) {
                case 'Sequence':
                    this.inputFilename = param.value;
                    break;
                case 'bin num':
                    this.bin_num = param.value;
                    break;
                case 'exclude':
                    this.exclude = param.value;
                    break;
                case 'exclude_max':
                    this.exclude_max = param.value;
                    break;
                case 'method':
                    this.method = param.value;
                    break;
                case 'kmer_len':
                    this.kmer_len = param.value;
                    break;
            }
        }
    }

    exec() {
        console.log('Running AbundanceBin Tool...', this.inputFilename);
       

        //`${__dirname} = D:\Project_Bin\EasyBin\EayBin-backend\python-wrappers\abundanceBin
        //const filepath = path.join(`${__dirname}`, '../../uploads')

        return new Promise((resolve, reject) => {

            //Divide the input file into two bins
        //if bin number is given then it should be used to bin, else -RECURSIVE_CLASSIFICATION should be used
           
            const inputFilename =  this.inputFilename;
            const command = `./abundancebin -input ../../uploads/${this.inputFilename} -output bins/${this.inputFilename} -bin_num 2`;
            console.log("print command");
            console.log(command);
 
            // Execute abundancebin command
            try {
                process.chdir('python-wrappers/abundanceBin');
                console.log(`New directory: ${process.cwd()}`);
              
                    cmd.get(command, (err, data, stderr) => {

                        if (err || stderr) {
                            reject("Abundancebin Error\n" + err);
                        } else {

                            const bin_files = [];
                            
                            var i = 1;
                            while (i <= 2) {
                                //const content = fs.readFileSync(`${__dirname}/${this.inputFilename}.out.${i}`, 'utf8');
                                bin_files.push(`${__dirname}/bins/${this.inputFilename}.${i}`)
                                i++;
                            }
                            bin_files.push(`${__dirname}/bins/${inputFilename}.unclassified`)

                            const bin_feature = `${__dirname}/bins/${inputFilename}.bin_feature`;

                            console.log(bin_files);

                            // //return Promise object 
                            resolve({ step: 'Abundancebin', output:  { bins: bin_files, bin_feature: bin_feature}  });
                        }

                });
                process.chdir('../../');
                console.log(`dir name-->${__dirname}`);
        }
        catch (err) {
            console.error(`chdir: ${err}`);
          }
        
        // const command = `cd db`

        // cmd.get(command, (err, data, stderr) => {

        //     if (err || stderr) {
        //         console.log("Abundancebin Error\n" + err);

        //     } else {
        //         console.log("in dir");
        //         fs.writeFileSync(`${__dirname}/db/no.fasta`, this.input);
        //     }

        // return new Promise(resolve => resolve({ step: 'Abundancebin', output: 'AbundanceBin Results' }));
        // });
    });
}
    
    /*  exec() {
         // Get input file name
         const inputFilename = uuidv4();
 
         // Create input file with input data
         fs.writeFileSync(`${__dirname}/${inputFilename}.fasta`, this.input);
 
         // Get output file name
         const outputFilename = uuidv4();
 
         return new Promise((resolve, reject) => {
 
             //Divide the input file into two bins
             const command = `abundancebin -input ${__dirname}/${inputFilename}.fasta -bin_num 2`
 
             // Execute abundancebin command
             cmd.get(command, (err, data, stderr) => {
 
                 if (err || stderr) {
                     reject("Abundancebin Error\n" + err);
                 } else {
                     const bin_files = [];
                     i = 1;
                     while (i <= 2) {
                         const content = fs.readFileSync(`${__dirname}/${inputFilename}.out.${i}`, 'utf8');
                         bin_files.push(content)
                         i++;
                     }
                     bin_files.push(fs.readFileSync(`${__dirname}/${inputFilename}.out.unclassified`, 'utf8'))
 
                     const bin_feature = fs.readFileSync(`${__dirname}/${inputFilename}.out.bin_feature`, 'utf8');
 
                     //return Promise object 
                     resolve({ step: 'Abundancebin', output: { bins: bin_files, bin_feature: bin_feature } });
                 }
 
                 // Remove input file
                 fs.existsSync(`${__dirname}/${inputFilename}.fasta`) && fs.unlinkSync(`${__dirname}/${inputFilename}.fasta`);
 
                 // Remove output files
                 fs.existsSync(`${__dirname}/${inputFilename}.out`) && fs.unlinkSync(`${__dirname}/${inputFilename}.out`);
                 j = 1;
                 while (j <= 2) {
                     fs.existsSync(`${__dirname}/${inputFilename}.out.${i}`) && fs.unlinkSync(`${__dirname}/${inputFilename}.out.${i}`);
                     j++;
                 }
                 fs.existsSync(`${__dirname}/${inputFilename}.out.unclassified`) && fs.unlinkSync(`${__dirname}/${inputFilename}.out.unclassified`);
                 fs.existsSync(`${__dirname}/${inputFilename}.out.bin_feature`) && fs.unlinkSync(`${__dirname}/${inputFilename}.out.bin_feature`);
                 fs.existsSync(`${__dirname}/${inputFilename}.out.count`) && fs.unlinkSync(`${__dirname}/${inputFilename}.out.count`);
 
                 console.log('output file removed');
             }
             );
 
         });
 
     } */
}
