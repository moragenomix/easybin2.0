import cmd from 'node-cmd';
import fs from 'fs';
import path from 'path';

export default class WrapCheckM {

    input;
    
    // TODO these are the parameters
    constructor(inp = null) {
        this.input = inp;

        console.log(this.input);
    }

    exec(){
        console.log('Running checkM Tool...');


        return new Promise((resolve, reject) => {

            var str = ``;

            for (let param of this.input) {
                str = str + `ln -s ${param.output.bins} input\n`
            }



            const command = `${str}` + 'checkm gc_plot -x fasta input visualise_output 95\ncheckm lineage_wf -t 8 -x fasta input output'            ;
            console.log(command);

            // const command = `
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/maxBin/bins/maxBin_output/maxBin/ input_bin_folder
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/concoct/bins/concoct_output/concoct/ input_bin_folder
            // Binning_refiner -i input_bin_folder -p Sample -plot
            // `;

            
            console.log(command);
            // Execute abundancebin command 
            try {
                console.log(`This directory: ${process.cwd()}`);
                process.chdir('python-wrappers/checkM/checkm/test');
                console.log(`New directory: ${process.cwd()}`);

                cmd.get(command, (err, data, stderr) => {

                    if (err == null || stderr == null) {
                        console.log("No Errors!");

                        const bin_files = `${process.cwd()}/visualise_output/`;
                        var i = 1;
                        while (i <= 2) {
                            
                            bin_files.push(`${process.cwd()}/visualise_output/Sample_${i}.gc_plots`);
                            i++;
                        }
                        // //return Promise object 
                        resolve({ step: 'checkM', output:  { bins: bin_files}  });

                        

                        
                        


                        // //return Promise object 
                       

                    } else {
                        reject("Bin refiner Error\n" + err);

                    }

                });
                process.chdir('../../../../');
            }
            catch (err) {
                console.error(`chdir: ${err}`);
            }
        });
        
    }
}