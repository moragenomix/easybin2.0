import cmd from 'node-cmd';
import fs from 'fs';
import path from 'path';
export default class WrapBinRefiner {
    input;

    // TODO these are the parameters
    constructor(inp = null) {
        // const sample_inp = [{
        //     step: 'Maxbin',
        //     output:
        //     {
        //         bins:
        //             '/home/nethma/FYP/easybin/EayBin-backend/python-wrappers/maxBin/bins/maxBin_output/maxBin/'
        //     }
        // },
        // {
        //     step: 'ConCoct',
        //     output:
        //     {
        //         bins:
        //             '/home/nethma/FYP/easybin/EayBin-backend/python-wrappers/concoct/bins/concoct_output/concoct/'
        //     }
        // }];
        // this.input = sample_inp;

        this.input = inp;
        console.log(this.input);
    }

    exec() {
        console.log('Running BinRefiner Tool...');


        return new Promise((resolve, reject) => {

            var str = ``;

            for (let param of this.input) {
                str = str + `ln -s ${param.output.bins} input_bin_folder\n`
            }



            const command = `${str}` + `Binning_refiner -i input_bin_folder -p Sample -plot`;
            console.log(command);

            // const command = `
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/maxBin/bins/maxBin_output/maxBin/ input_bin_folder/
            // ln -s /home/nethma/FYP/easybin/EayBin-backend/python-wrappers/concoct/bins/concoct_output/concoct/ input_bin_folder/
            // Binning_refiner -i input_bin_folder -p Sample -plot
            // `;

            
            console.log(command);
            // Execute abundancebin command 
            try {
                console.log(`This directory: ${process.cwd()}`);
                process.chdir('python-wrappers/bin_refiner');
                console.log(`New directory: ${process.cwd()}`);

                cmd.get(command, (err, data, stderr) => {

                    if (err == null || stderr == null) {
                        console.log("No Errors!");

                        

                        const bin_files = `${process.cwd()}/Sample_Binning_refiner_outputs/Sample_refined_bins/`;
                        // var i = 1;
                        // while (i <= 2) {
                        //     //const content = fs.readFileSync(`${__dirname}/${this.inputFilename}.out.${i}`, 'utf8');
                        //     bin_files.push(`${process.cwd()}/concoct_output/concoct/myout.00${i}`)
                        //     i++;
                        // }
                        // //return Promise object 
                        


                        // //return Promise object 
                        resolve({ step: 'BinRefiner', output: {bins: bin_files } });

                    } else {
                        reject("Bin refiner Error\n" + err);

                    }
                    process.chdir('../../');

                });
            }
            catch (err) {
                console.error(`chdir: ${err}`);
            }
        });
    }



}
