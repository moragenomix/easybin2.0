
import cmd from 'node-cmd';
import fs from 'fs';
import path from 'path';
import uuidv4 from 'uuid/v4';

export default class WrapMaxbin {
    inputContigFilename;
    inputReadFilename1;
    inputReadFilename2;

    // TODO these are the parameters
    constructor(inp = null) {
        for (let param of inp) {
            console.log("obejct-->", param);
            switch (param.name) {
                case 'ContigFile':
                    this.inputContigFilename = param.value;
                    break;
                case 'ReadFile_1':
                    this.inputReadFilename1 = param.value;
                    break;
                case 'ReadFile_2':
                    this.inputReadFilename2 = param.value;
                    break;

            }
        }
    }

    exec() {
        console.log('Running Maxbin Tool...', this.inputContigFilename);


        //`${__dirname} = D:\Project_Bin\EasyBin\EayBin-backend\python-wrappers\abundanceBin
        //const filepath = path.join(`${__dirname}`, '../../uploads')

        return new Promise((resolve, reject) => {

            //Divide the input file into two bins

            const inputContigFilename = this.inputContigFilename;
            const inputReadFilename1 = this.inputReadFilename1;
            const inputReadFilename2 = this.inputReadFilename2;


            const command = `
            run_MaxBin.pl -contig ../../../uploads/${this.inputContigFilename} -reads ../../../uploads/${this.inputReadFilename1} -reads2 ../../../uploads/${this.inputReadFilename2} -out maxBin_output/myout`;

            console.log(command);
            // Execute abundancebin command
            try {
                console.log(`This directory: ${process.cwd()}`);
                process.chdir('python-wrappers/maxBin/bins');
                console.log(`New directory: ${process.cwd()}`);

                cmd.get(command, (err, data, stderr) => {

                    if (err == null || stderr == null) {
                        console.log('No errors');

                        

                        var dirPath = `${process.cwd()}/maxBin_output`;  //directory path
                        var fileType = `.fasta`; //file extension

                        if (!fs.existsSync(`${dirPath}/maxBin/`)){
                            fs.mkdirSync(`${dirPath}/maxBin/`);
                        }
                        var filenames = fs.readdirSync(dirPath); 

                        filenames.forEach(file => { 
                            if (path.extname(file) === fileType) {
                                console.log(`move file to ------>${dirPath}/maxBin/${file}`);
                                fs.renameSync(`${dirPath}/${file}`, `${dirPath}/maxBin/${file}`, (err) => {
                                    if (err) throw err;
                                });
                                console.log("Files moved"); //print the file
                            }
                          });

                        // fs.readdirSync(dirPath, function (err, list) {
                        //     if (err) throw err;
                        //     for (var i = 0; i < list.length; i++) {
                        //         if (path.extname(list[i]) === fileType) {
                        //             fs.renameSync(`${dirPath}/${list[i]}`, `${dirPath}/maxBin/${list[i]}`, (err) => {
                        //                 if (err) throw err;
                        //             });
                        //             console.log("Files moved"); //print the file
                        //         }
                        //     }
                        // });

                        const bin_files = `${dirPath}/maxBin/`;

                        resolve({ step: 'Maxbin', output: { bins: bin_files } });

                    } else {
                        reject("Maxbin Error\n" + err);

                    }
                    process.chdir('../../../');

                });
            }
            catch (err) {
                console.error(`chdir error : ${err}`);
            }
        });
    }
}