import cmd from 'node-cmd';
import fs from 'fs';
import uuidv4 from 'uuid/v4';


export default class WrapConcoct {
    // Get input file name
    inputContigFilename;
    inputReadFilename1;
    inputReadFilename2;

    // TODO these are the parameters
    constructor(inp = null) {
        for (let param of inp) {
            console.log("obejct-->", param);
            switch (param.name) {
                case 'ContigFile':
                    this.inputContigFilename = param.value;
                    break;
                case 'ReadFile_1':
                    this.inputReadFilename1 = param.value;
                    break;
                case 'ReadFile_2':
                    this.inputReadFilename2 = param.value;
                    break;
               
            }
        }
    }


    exec() {
        console.log('Running ConCoct Tool...', this.inputContigFilename);

        // Get output file name
        const outputFilename = uuidv4();

        return new Promise((resolve, reject) => {

            try {
                console.log(`This directory: ${process.cwd()}`);
                process.chdir('python-wrappers/concoct/bins');
                console.log(`New directory: ${process.cwd()}`);


                 const command = `
                    ln -s ../../../uploads/${this.inputReadFilename1} pair1.fastq
                    ln -s ../../../uploads/${this.inputReadFilename2}  pair2.fastq
                    ln -s ../../../uploads/${this.inputContigFilename} contigs.fa
                    bowtie2-build contigs.fa contigs.fa
                    bowtie2 -p 8 -x contigs.fa -1 pair1.fastq -2 pair2.fastq -S out.map.sam
                    samtools sort -o out.map.sorted.bam -O bam out.map.sam
                    samtools index out.map.sorted.bam
                    conda run -n concoct_env cut_up_fasta.py contigs.fa -c 10000 -o 0 --merge_last -b contigs_10K.bed > contigs_10K.fa
                    conda run -n concoct_env concoct_coverage_table.py contigs_10K.bed out.map.sorted.bam > coverage_table.tsv
                    conda run -n concoct_env concoct --composition_file contigs_10K.fa --coverage_file coverage_table.tsv -b concoct_output/
                    conda run -n concoct_env merge_cutup_clustering.py concoct_output/clustering_gt1000.csv > concoct_output/clustering_merged.csv
                    mkdir concoct_output/concoct
                    conda run -n concoct_env extract_fasta_bins.py contigs.fa concoct_output/clustering_merged.csv --output_path concoct_output/concoct
                    `;


                // Execute command
                cmd.get(command, (err, data, stderr) => {

                    if (err == null || stderr == null) {
                        console.log("No Errors");

                        const bin_files = `${process.cwd()}/concoct_output/concoct/`;
                        // var i = 1;
                        // while (i <= 2) {
                        //     //const content = fs.readFileSync(`${__dirname}/${this.inputFilename}.out.${i}`, 'utf8');
                        //     bin_files.push(`${process.cwd()}/concoct_output/concoct/myout.00${i}`)
                        //     i++;
                        // }
                        // //return Promise object 
                        resolve({ step: 'ConCoct', output:  { bins: bin_files}  });

                    } else {
                        console.log("Concoct Error\n" + err);
                        reject("Concoct Error\n" + err);
                    }

                    // Remove input file
                    process.chdir('../../../');

                }
                );





                // process.chdir('../../');
                // console.log(`dir name-->${__dirname}`);
            }
            catch (err) {
                console.error(`chdir error : ${err}`);
            }



        });
    }

}