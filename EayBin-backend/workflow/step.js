//  export default class Step {
//     nextStep = null;
//     isInitial;
//     task;
//     data;

//     constructor(task, data = null, isInitial = false) {
//         this.isInitial = isInitial;
//         this.task = task;
//         this.data = data;
//     }

//     setNext(step) {
//         this.nextStep = step;
//     }

//     async start(data = null) {
//         const result = await this.task.execute(data || this.data);
//         if (this.nextStep) {
//             return await this.nextStep.start(result);
//         } else {
//             return result;
//         }
//     }
// }

export default class Step {
    stepId;
    name;
    params = [] ;
    nextStepIds = [];
    parentStepIds = [];
    computedValue = null;
    parentResults = [];

    constructor(step) {
        this.stepId = step.stepId;
        this.name = step.name;

        //add nextStepIds
        if(typeof step.nextStepIds !== 'undefined'){
            this.nextStepIds = step.nextStepIds;
        }

        //add parentStepIds
        if(typeof step.parentStepIds !== 'undefined'){
            this.parentStepIds = step.parentStepIds;
        }

        //add params
        if(typeof step.params !== 'undefined'){
            this.params = step.params;
        }
    }

}