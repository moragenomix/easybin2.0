/**
 * Created by anuradhawick on 2/26/18.
 */
import * as _ from 'lodash';

import * as Task from '../workflow/tasks';

export default function run(step) {
    switch (step.name) {
        case 'User Input':
            return (new Task.Data(step.params[0].value)).execute();
        case 'AbundanceBin':
            return (new Task.AbundanceBin()).execute(step.params);
        case 'MaxBin':
            return (new Task.MaxBin()).execute(step.params);
        case 'CONCOCT':
            return (new Task.ConCoct()).execute(step.params);
        case 'Bin_refiner':
            return (new Task.BinRefiner()).execute(step.parentResults);
        case 'CheckM':
            return (new Task.CheckM()).execute(step.parentResults);
        case 'Visualizer':
            return (new Task.Visualizer()).execute(step.parentResults[0]);
    }
}
