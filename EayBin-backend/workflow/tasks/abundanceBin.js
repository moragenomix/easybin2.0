import Wrap_abundanceBin from '../../python-wrappers/abundanceBin/wrap_abundanceBin';

export default class AbundanceBin {

    constructor() {}

    execute(taskParams) {
        const wrap = new Wrap_abundanceBin(taskParams);
        return wrap.exec();
    }
}