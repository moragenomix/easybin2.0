import Wrap_visualizer from '../../python-wrappers/visualizer/wrap_visualizer';

export default class Visualizer{

    constructor() {}

    execute(...taskParams) {
        const wrap = new Wrap_visualizer(taskParams[0].output);
        return wrap.exec();
    }
}