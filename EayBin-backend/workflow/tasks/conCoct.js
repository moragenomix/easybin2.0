import Wrap_concoct from '../../python-wrappers/concoct/wrap-concoct';

export default class ConCoct {

    constructor() {}

    execute(taskParams) {
        const wrap = new Wrap_concoct(taskParams);
        return wrap.exec();
    }
}