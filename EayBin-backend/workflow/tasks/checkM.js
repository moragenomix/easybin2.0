import Wrap_checkM from '../../python-wrappers/checkM/wrap_checkM';

export default class CheckM {

    constructor() {}

    execute(...taskParams) {
        const wrap = new Wrap_checkM(taskParams[0]);
        return wrap.exec();
    }
}