import Wrap_binRefiner from '../../python-wrappers/bin_refiner/wrap_binRefiner';

export default class BinRefiner {

    constructor() {}

    execute(...taskParams) {
        const wrap = new Wrap_binRefiner(taskParams[0]);
        return wrap.exec();
    }
}