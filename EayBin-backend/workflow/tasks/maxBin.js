import Wrap_maxBin from '../../python-wrappers/maxBin/wrap_maxBin';

export default class MaxBin{

    constructor() {}

    execute(taskParams) {
        const wrap = new Wrap_maxBin(taskParams);
        return wrap.exec();
    }
}