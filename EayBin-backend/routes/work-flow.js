import express from "express";
import * as _ from 'lodash';
import uuidv4 from 'uuid/v4';
import path from 'path';

import step_executor from '../workflow/step-executor';

import Step from '../workflow/step';
import * as Task from '../workflow/tasks';
import shortid from 'shortid';
import AdmZip from 'adm-zip';
import zip from 'express-easy-zip';


import multer from "multer";

var storage = multer.diskStorage(
    {
        destination: 'uploads/',
        filename: function (req, file, cb) {
            cb(null, uuidv4() + path.parse(file.originalname).ext);
        }
    }
);

var upload = multer({
    storage: storage,
}).any('value');

const router = express.Router();

router.use((req, res, next) => {
    next()
});

router.use(zip());

var incompleteList = [];
var completedList = [];
var RunningList = [];

router.post('/flow', async (req, res) => {
    res.header('content-type', 'application/json');

    incompleteList = [];
    completedList = [];
    RunningList = [];

    upload(req, res, async (err) => {
        if (err) {
            console.log(err);
        }

        console.log(req.files);

        for (let step of req.body.steps) {
            incompleteList.push(new Step(step));
        }

        for (let file of req.files) {
            var str = file.fieldname;
            var stepIndex = str.match(/steps\[(.*?)\]/i)[1];
            var paramIndex = str.match(/\[params\]\[(.*?)\]/i)[1];
            incompleteList[stepIndex].params[paramIndex].value = file.filename;
        }

        while (hasRunnable()) {
            const runnableList = getRunnables();
            let step = null;

            for (let i = 0; i < runnableList.length; i++) {
                step = runnableList[i];
                RunningList.push(step);
                await executeStep(step);
                RunningList = RunningList.filter((item) => item !== step)
                completedList.push(step);
                console.log("completed ", step.name);
            }
        }

        res.status(200).send(_.map(completedList, (step) => {
            return {
                name: step.name,
                stepId: step.stepId,
                computedValue: step.computedValue
            }
        }));

        console.log("WorkFlow Done");

    });

    function hasRunnable() {
        const result = _.find(incompleteList, (step) => {
            return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
        });
        return !_.isEmpty(result); //return true if result is not empty
    }

    function getRunnables() {
        return _.remove(incompleteList, (step) => {
            return step.parentStepIds.length === 0 || step.parentStepIds.length === step.parentResults.length;
        });
    }

    async function executeStep(step) {
        // execute and obtain result
        const result = await step_executor(step);
        step.computedValue = result;

        // propagate results for children
        const nextStepIds = step.nextStepIds;
        _.each(nextStepIds, (id) => {
            const stepObj = _.find(incompleteList, (obj) => id === obj.stepId);
            stepObj.parentResults.push(step.computedValue);
        });
    }
});

router.get('/status', (req, res) => {
    res.header('content-type', 'application/json');

    console.log("checking status");

    var results = [
        {
            status: "Completed",
            steps: _.map(completedList, (step) => {
                return {
                    name: step.name,
                    stepId: step.stepId,
                    output : step.computedValue
                }
            })
        },
        {
            status: "Running",
            steps: _.map(RunningList, (step) => {
                return {
                    name: step.name,
                    stepId: step.stepId,
                    output : step.computedValue
                }
            })
        },
        {
            status: "Incomplete",
            steps: _.map(incompleteList, (step) => {
                return {
                    name: step.name,
                    stepId: step.stepId,
                    output : step.computedValue
                }
            })
        }
    ]

    res.status(200).send({ result: results });
});

router.get('/download', function (req, res, next) {
    // var dirPath = __dirname + "/../public/uploads";
    console.log("Downloading folder");
    var dirPath = req.param('path');
    res.zip({
        files: [
            { path: dirPath, name: 'results' }
        ],
        filename: `${Date.now()}.zip`
    });
});

module.exports = router;