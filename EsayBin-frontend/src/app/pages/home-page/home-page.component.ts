import { Component, OnInit, ViewChild } from '@angular/core';

import { TreeViewComponent } from '../../components/tree-view/tree-view.component';
import { DrawingBoardComponent } from '../../components/drawing-board/drawing-board.component';
import { ExecuteFlowService } from '../../services/work-flow/execute-flow.service';
import { PairwiseBlastComponent } from '../../components/visualizers/pairwise-blast/pairwise-blast.component';
import { ClustalOmegaMsaComponent } from '../../components/visualizers/clustal-omega-msa/clustal-omega-msa.component';
import { DialignMsaComponent } from '../../components/visualizers/dialign-msa/dialign-msa.component';
import { TCoffeeMsaComponent } from '../../components/visualizers/t-coffee-msa/t-coffee-msa.component';

import * as _ from 'lodash';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [
    TreeViewComponent,
    DrawingBoardComponent,
    ExecuteFlowService,
    PairwiseBlastComponent
  ]
})
export class HomePageComponent implements OnInit {
  @ViewChild(DrawingBoardComponent) drawingBoard: DrawingBoardComponent;
  @ViewChild(PairwiseBlastComponent) pairwiseBlasterView: PairwiseBlastComponent;
  @ViewChild(ClustalOmegaMsaComponent) clustalOmegaView: ClustalOmegaMsaComponent;
  @ViewChild(DialignMsaComponent) dialignView: DialignMsaComponent;
  @ViewChild(TCoffeeMsaComponent) tCoffeeView: TCoffeeMsaComponent;


  private activeTreeItem: any = null;
  private readyWorkflow: any = null;
  private activeWorkflow: any = null;
  private finishedWorkflow: any = null;
  private resultData: any = null;
  private statusData: any = null;

  constructor(private executionService: ExecuteFlowService) {
  }

  ngOnInit() {
    this.readyWorkflow = false;
    this.activeWorkflow = false;
    this.finishedWorkflow = false;
  }

  selectTree(item) {
    this.activeTreeItem = item;
  }

  addStep() {
    (this.activeTreeItem.isStep && !this.activeWorkflow) && this.drawingBoard.addStep(this.activeTreeItem);
    this.readyWorkflow = true;
  }

  execute() {
    // this.clearUI();
    this.finishedWorkflow = false;
    const sequence = this.drawingBoard.getStepSequence();
    if (sequence.length > 0) {
      console.log(sequence);
      this.activeWorkflow = true;
      this.drawingBoard.setActive();

      this.executionService.executeFlow(sequence).subscribe((results) => {
        this.resultData = results;
        console.log('results came ', results);
        this.activeWorkflow = false;
        this.drawingBoard.offActive();
        this.finishedWorkflow = true;
        this.checkStatus();
        // _.each(results, (result: any) => {
        //   console.log(result.name)
        //   if (result.name === 'Visualize Output') {
        //     console.log(result.computedValue)
        //     _.each(result.computedValue.data, (visualizeData:any) => {
        //       switch (visualizeData.step) {
        //         case 'blast':
        //           this.pairwiseBlasterView.render(visualizeData.text);
        //           break;
        //         case 'clustal-omega':
        //           this.clustalOmegaView.render(visualizeData.output);
        //           break;
        //         case 'clustal-omega-max-align':
        //           this.clustalOmegaView.render(visualizeData.output);
        //           break;
        //         case 'dialign':
        //           this.dialignView.render(visualizeData.output);
        //           break;
        //         case 't-coffee':
        //           this.tCoffeeView.render(visualizeData.output);
        //           break;
        //       }
        //     });
        //   }
        // });
      });
    }

  }

    // var sample_result = {
        //   result: [{
        //     status: "Completed",
        //     stepId: "step.stepId",
        //     steps: [{ name: "AbundanceBin", output: { step: 'Abundancebin', output: { bins: "D:/Test" } } }]
        //   },
        //   {
        //     status: "Running",
        //     steps: [{ name: "Bin_refiner", output : null }]
        //   },
        //   {
        //     status: "Incomplete",
        //     steps: [
        //       { name: "AbundanceBin", output : null },
        //       { name: "Bin_refiner", output : null },
        //       { name: "CheckM", output : null },
        //     ]
        //   }]
        // }


  checkStatus() {

    if (this.activeWorkflow || this.finishedWorkflow) {
      // document.getElementById("overlay").style.display = "block";
      this.executionService.getStatus().subscribe((results) => {
        console.log('Status Check', results.result);
      
        this.statusData = [];
        for (let item of results.result) {
          for (let step of item.steps) {
            if (step.output){
              this.statusData.push({ step: step.name, stepId: step.stepId, status: item.status, output: step.output.output.bins });
            }else{
              this.statusData.push({ step: step.name, stepId: step.stepId, status: item.status, output: null });
            }
          }
        }
        console.log("statusData", this.statusData);
        this.drawingBoard.addStatus(this.statusData);
        console.log("statusData add");
      });
    }

   }

  // clearUI() {
  //   this.pairwiseBlasterView.clear();
  //   this.clustalOmegaView.clear();
  //   this.dialignView.clear();
  //   this.tCoffeeView.clear();
  // }

  // off() {
  //   document.getElementById("overlay").style.display = "none";
  // }

}
