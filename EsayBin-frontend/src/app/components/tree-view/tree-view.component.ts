import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TreeViewService } from '../../services/support/tree-view.service';

declare const $: any;

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css'],
  providers: [TreeViewService]
})
export class TreeViewComponent implements OnInit {
  @Output() select: EventEmitter<any> = new EventEmitter();

  private services: any = {
    'core': {
      'data': [
        // {
        //   "text": "Input",
        //   "id": "I1",
        //   "precedence": "1",
        //   "isStep": true,
        //   "description": "Insert your file",
        //   "inputs": [
        //     {
        //       "name": "Sequence",
        //       "type": "file",
        //       "value": []
        //     }
        //   ]
        // },
        {
          'text': 'Binning',
          'state': {
            'opened': true,
          },
          'description': 'Grouping reads and contigs into clusters',
          'children': [
            {
              "Id": "B1",
              "text": "AbundanceBin",
              "isStep": true,
              "precedence": "2",
              "inputs": [
                {
                  "name": "Sequence",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "bin num",
                  "type": "text",
                  "value": ""
                },
                {
                  "name": "kmer_len",
                  "type": "text",
                  "value": ""
                },
                {
                  "name": "exclude",
                  "type": "text",
                  "value": ""
                },
                {
                  "name": "exclude_max",
                  "type": "text",
                  "value": ""
                },
                {
                  "name": "Recursive",
                  "type": "check",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Bin reads or contigs by taxonomy free binning"
            },
            {
              "Id": "B2",
              "text": "MaxBin",
              "isStep": true,
              "precedence": "2",
              "inputs": [

                {
                  "name": "ContigFile",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_1",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_2",
                  "type": "file",
                  "value": ""
                }

              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Bin reads and contigs by taxonomy free binning"
            },
            {
              "Id": "B3",
              "text": "MataBAT",
              "isStep": true,
              "precedence": "2",
              "inputs": [
                {
                  "name": "ContigFile",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_1",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_2",
                  "type": "file",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Bin reads and contigs by taxonomy free binning"
            },
            {
              "Id": "B4",
              "text": "CONCOCT",
              "isStep": true,
              "precedence": "2",
              "inputs": [
                {
                  "name": "ContigFile",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_1",
                  "type": "file",
                  "value": ""
                },
                {
                  "name": "ReadFile_2",
                  "type": "file",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Bin reads and contigs by taxonomy free binning"
            }
          ]
        },
        {
          'text': 'Refining',
          'state': {
            'opened': true,
          },
          'description': 'Improving bins created by binning tools',
          'children': [
            {
              "Id": "R1",
              "text": "Bin_refiner",
              "isStep": true,
              "precedence": "3",
              "inputs": [
                {
                  "name": "minimal size",
                  "type": "text",
                  "value": "512"
                },
                {
                  "name": "visualize ",
                  "type": "check",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Improve bins using bin_refiner"
            }
          ]
        },
        {
          'text': 'Evaluation',
          'state': {
            'opened': true,
          },
          'description': 'Evaluate bins created by binning tools',
          'children': [
            {
              "Id": "E1",
              "text": "CheckM",
              "isStep": true,
              "precedence": "4",
              "inputs": [
                {
                  "name": "Reference Tree",
                  "type": "select",
                  "selectors": [
                    "Reduced tree",
                    "full tree"
                  ],
                  "value": ""
                },
                {
                  "name": "Save all plots ",
                  "type": "check",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Improve bins using bin_refiner"
            }
          ]
        },
        {
          'text': 'Visualization',
          'state': {
            'opened': true,
          },
          'description': 'Visualizing results',
          'children': [
            {
              "Id": "V1",
              "text": "Visualizer",
              "isStep": true,
              "precedence": "5",
              "inputs": [
                {
                  "name": "Undefined",
                  "type": "check",
                  "value": ""
                }
              ],
              "OutputParams": {
                "output": ""
              },
              "description": "Visualize results using this"
            }
          ]
        }
      ]
    }
  };

  constructor() {
  }

  ngOnInit() {
    // this.treeService.getTree().then((res) => {
    //   this.services.core.data = res;
    //   this.storage.saveComponentTree(res);
    // });
    this.initTree();
  }

  initTree() {
    $('#jstree').jstree(this.services).on('changed.jstree', (e, data) => {
      this.select.emit(data.node.original);
    }).jstree();
  }

}
