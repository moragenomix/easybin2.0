import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/toPromise';

import * as config from '../../configs/index';

@Injectable()
export class ExecuteFlowService {
  // private formData: FormData;


  constructor(private http: HttpClient) {
  }

  // takes a {} object and returns a FormData object
  createFormData(object: Object, form?: FormData, namespace?: string): FormData {
    const formData = form || new FormData();
    for (let property in object) {
      if (!object.hasOwnProperty(property) || !object[property]) {
        continue;
      }
      const formKey = namespace ? `${namespace}[${property}]` : property;

      if (object[property] instanceof Date) {
        formData.append(formKey, object[property].toISOString());

      } else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
        this.createFormData(object[property], formData, formKey);

      } else {
        formData.append(formKey, object[property]);
      }
    }
    return formData;
  }

  // Const httpOptions = {
  //   headers : new HttpHeaders({
  //     'Content-Type' : 'application/json';
  //   })
  // }
  executeFlow(steps: any): Observable<any> {

    // const headers: Headers = new Headers();
    const formData = this.createFormData({ steps: steps });

    for (let [key, value] of formData.entries()) {
      console.log(key, value);
    }

    return this.http.post(
      `${config.apiUrl}/api/flow`, formData
    );
  }

  // executeFlow(steps) {
  //   const headers: Headers = new Headers();
  //   const formData = this.createFormData({steps: steps});

  //   for (let [key, value] of formData.entries()) { 
  //     console.log(key, value);
  //   }

  //   // return this.http.post(
  //   //   `${config.apiUrl}/api/flow`, formData,
  //   //   {
  //   //     headers: headers
  //   //   })

  // }

  getStatus(): Observable<any> {
    return this.http.get(
      `${config.apiUrl}/api/status`
    );
  }

  // getStatus() {
  //   const headers: Headers = new Headers();

  //   return this.http.get(
  //     `${config.apiUrl}/api/status`, 
  //     {
  //       headers: headers
  //     }).toPromise().then(response => response.json())
  // }

}

