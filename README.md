# EasyBin - Scientific Workflow Management System for Metagenomic Binning
A EasyBin is a scientific workflow management system designed specifically to compose and execute a series of computational or data manipulation steps that is related to metagenomics in a very user-friendly manner. 

## Getting Started
Clone the repositiory
### Frontend
1. Go to EsayBin-frontend directory.	
2. Get all the dependencies using `npm install` command.
3. Run `ng serve --open` command.
### Backend
1.	Go to EsayBin-backend directory.
2. Get all the dependencies using `npm install` command.
3. Run `npm start` command.
  